/*
AUTHOR: Joao Victor Lopes Dias
DATE: 2022/12/14
TITLE: String és igual
 */

import java.util.Scanner

fun main(){
    val scanner = Scanner(System.`in`)
    val wordOne = scanner.next()
    val wordTwo = scanner.next()

    println(isEqual(wordOne,wordTwo))
}

fun isEqual(entry1: String, entry2: String): Boolean{
    return entry1 == entry2
}
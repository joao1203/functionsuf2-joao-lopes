/*
AUTHOR: Joao Victor Lopes Dias
DATE: 2022/12/14
TITLE: Divideix un String
*/

import java.util.Scanner

fun main(){
    val scanner = Scanner(System.`in`)
    val string = scanner.next()
    val separator = scanner.next().single()

    println(divideString(string,separator))
}

fun divideString(word: String, separator: Char): List<String>{
    val list = mutableListOf<String>()
    var empty = ""

    for (i in word.indices){
        if(word[i] == separator || i == word.lastIndex){
            if(i == word.lastIndex && word[i] != separator) empty += word[i]
            list.add(empty)
            empty = ""
        }
        else empty += word[i]
    }

    return list
}
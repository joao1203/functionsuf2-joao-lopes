/*
AUTHOR: Joao Victor Lopes Dias
DATE: 2022/12/14
TITLE: D’String a MutableList<Char>
*/

import java.util.Scanner

fun main(){
    val scanner = Scanner(System.`in`)
    val string = scanner.nextLine()

    stringToList(string)
}

fun stringToList(value: String){
    val list = mutableListOf<Char>()
    for(i in value){
        list.add(i)
    }
    println(list)
}
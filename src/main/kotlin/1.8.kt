/*
AUTHOR: Joao Victor Lopes Dias
DATE: 2022/12/14
TITLE: Escriu una línia
*/

import java.util.Scanner

fun main(){
    val scanner = Scanner(System.`in`)
    val num1 = scanner.nextInt()
    val num2 = scanner.nextInt()
    val char = scanner.next().single()

    writeLine(num1,num2,char)
}

fun writeLine(value1: Int, value2: Int, letter:Char){
    repeat(value1){
        print(" ")
    }
    repeat(value2){
        print("$letter")
    }
}
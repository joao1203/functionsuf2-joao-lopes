/*
AUTHOR: Joao Victor Lopes Dias
DATE: 2022/12/14
TITLE: Escriu un marc per un String
*/

import java.util.Scanner

fun main(){
    val scanner = Scanner(System.`in`)
    val phrase = scanner.nextLine()
    val char = scanner.next().single()

    borderPhrase(phrase,char)
}

fun borderPhrase(value: String, Type: Char){
    repeat(value.length+4){
        print("$Type")
    }
    println("")

    for(i in 1..value.length+5){
        if(i == 1 || i == value.lastIndex+5){
            print("$Type")
        }
        else{
            print(" ")
        }
    }
    println("")
    println("$Type $value $Type")
    for(i in 1..value.length+5){
        if(i == 1 || i == value.lastIndex+5){
            print("$Type")
        }
        else{
            print(" ")
        }
    }
    println("")
    repeat(value.length+4){
        print("$Type")
    }
}
/*
AUTHOR: Joao Victor Lopes Dias
DATE: 2022/12/14
TITLE: Caràcter d’un String
 */

import java.util.Scanner

fun main(){
    val scanner = Scanner(System.`in`)
    val string = scanner.nextLine()
    val choseChar = scanner.nextInt()

    println(charInString(string,choseChar))
}

fun charInString(phrase: String, value:Int): Any {
    if (value > phrase.length) return "La mida de l'String és inferior a $value"
    return phrase[value]
}
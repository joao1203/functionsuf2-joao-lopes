/*
AUTHOR: Joao Victor Lopes Dias
DATE: 2022/12/14
TITLE: Mida d'un String
 */

import java.util.Scanner

fun main(){
    val scanner = Scanner(System.`in`)
    val string = scanner.nextLine()

    println(stringLength(string))
}

fun stringLength(string: String): Int{
    var count = 0
    for(i in string){
        count++
    }
    return  count
}
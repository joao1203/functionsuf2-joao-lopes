/*
AUTHOR: Joao Victor Lopes Dias
DATE: 2022/12/14
TITLE: Eleva’l
*/

import java.util.Scanner

fun main(){
    val scanner = Scanner(System.`in`)
    val num1 = scanner.nextInt()
    val num2 = scanner.nextInt()

    println(powerNum(num1,num2))
}

fun powerNum(value1: Int, value2: Int): Int {
    var num = 1

    repeat(value2){
        num *= value1
    }
    return num
}
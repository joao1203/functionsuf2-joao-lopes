/*
AUTHOR: Joao Victor Lopes Dias
DATE: 2022/12/14
TITLE: Escriu una línia
*/

import java.util.Scanner

fun main(){
    val scanner = Scanner(System.`in`)
    val num = scanner.nextInt()
    val char = scanner.next().single()

    makeCross(num,char)
}

fun makeCross(num:Int, char: Char){
    for(i in 1..num){
        if(i == (num/2 + 1)){
            repeat(num){
                print(char)
            }
        }
        else{
            for(j in 1..num){
                if (j == (num/2 + 1)){
                    print(char)
                }
                else print(" ")
            }
        }
        println()
    }
}
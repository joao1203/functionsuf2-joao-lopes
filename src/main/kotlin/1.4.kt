/*
AUTHOR: Joao Victor Lopes Dias
DATE: 2022/12/14
TITLE: Subseqüència
 */

import java.util.Scanner

fun main(){
    val scanner = Scanner(System.`in`)
    val string = scanner.nextLine()
    val start = scanner.nextInt()
    val end = scanner.nextInt()

    subsequence(string,start,end)
}

fun subsequence(phrase: String, start:Int, end:Int){
    if (start > phrase.length || start > end){
        println("La subseqüència $start-$end de l'String no existe")
    }
    else{
        for(i in start..end){
            print(phrase[i])
        }
    }
}